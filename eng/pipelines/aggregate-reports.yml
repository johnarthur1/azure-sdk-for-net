trigger: none
pr: none

jobs:
  - job: GenerateReports

    variables:
      - template: templates/variables/globals.yml

    pool:
      vmImage: 'windows-2019'

    steps:
      - task: UseDotNet@2
        displayName: 'Use .NET Core sdk $(DotNetCoreSDKVersion)'
        inputs:
          version: '$(DotNetCoreSDKVersion)'

      - template: /eng/common/pipelines/templates/steps/verify-links.yml
        parameters:
          Directory: ""

      - pwsh: mkdir '$(System.ArtifactsDirectory)/BuildArtifacts'
        displayName: Create Artifact Directory

      - task: DownloadPipelineArtifact@2
        displayName: 'Download Pipeline Artifact'
        inputs:
          buildType: specific
          project: $(TargetPipelineProjectId)
          definition: $(TargetPipelineDefinition)
          buildVersionToDownload: latestFromBranch
          tags: scheduled
          artifactName: packages
          targetPath: '$(System.ArtifactsDirectory)/BuildArtifacts'

      - pwsh: |
          mkdir '$(System.ArtifactsDirectory)/Packages'
          Move-Item -Path '$(System.ArtifactsDirectory)/BuildArtifacts/*' -Destination '$(System.ArtifactsDirectory)/Packages/' -Include Azure.*.nupkg -Exclude *.symbols.nupkg
        displayName: Isolate packages to process

      - pwsh: |
          mkdir '$(Build.ArtifactStagingDirectory)/reports'
          Copy-Item -Path '$(Build.SourcesDirectory)/eng/common/InterdependencyGraph.html' -Destination '$(Build.ArtifactStagingDirectory)/reports/InterdependencyGraph.html'
        displayName: Setup reports directory

      - task: PowerShell@2
        displayName: Generate Dependency Report
        inputs:
          pwsh: true
          filePath: 'eng/scripts/dependencies/AnalyzeDeps.ps1'
          arguments: >
            -PackagesPath '$(System.ArtifactsDirectory)/Packages/'
            -LockfilePath '$(Build.SourcesDirectory)/eng/Packages.Data.props'
            -OutPath '$(Build.ArtifactStagingDirectory)/reports/dependencies.html'
            -DumpPath '$(Build.ArtifactStagingDirectory)/reports/data.js'

      - task: PowerShell@2
        displayName: 'Generate azure-sdk.deps.json'
        inputs:
          pwsh: true
          filePath: 'eng/scripts/dependencies/generate-deps.ps1'
          arguments: >
            -PackagesPath '$(System.ArtifactsDirectory)/Packages/'
            -DepsOutputFile '$(Build.ArtifactStagingDirectory)/reports/azure-sdk.deps.json'
            -ProjectRefPath '$(Build.ArtifactStagingDirectory)/reports'

      - task: PowerShell@2
        displayName: 'Validate dependencies with pwsh servicing'
        inputs:
          pwsh: true
          filePath: 'eng/scripts/dependencies/compare-deps-files.ps1'
          arguments: >
            -PSDepsFile 'https://aka.ms/ps-deps-servicing'
            -AzSdkDepsFile '$(Build.ArtifactStagingDirectory)/reports/azure-sdk.deps.json'

      - task: PowerShell@2
        displayName: 'Validate dependencies with pwsh stable'
        inputs:
          pwsh: true
          filePath: 'eng/scripts/dependencies/compare-deps-files.ps1'
          arguments: >
            -PSDepsFile 'https://aka.ms/ps-deps-stable'
            -AzSdkDepsFile '$(Build.ArtifactStagingDirectory)/reports/azure-sdk.deps.json'

      - task: PowerShell@2
        displayName: 'Validate dependencies with pwsh preview'
        inputs:
          pwsh: true
          filePath: 'eng/scripts/dependencies/compare-deps-files.ps1'
          arguments: >
            -PSDepsFile 'https://aka.ms/ps-deps-preview'
            -AzSdkDepsFile '$(Build.ArtifactStagingDirectory)/reports/azure-sdk.deps.json'

      - task: PublishPipelineArtifact@1
        displayName: 'Publish Report Artifacts'
        inputs:
          artifactName: reports
          path: '$(Build.ArtifactStagingDirectory)/reports'

      - task: AzureFileCopy@2
        displayName: 'Upload dependency report'
        inputs:
          sourcePath: '$(Build.ArtifactStagingDirectory)/reports'
          azureSubscription: 'Azure SDK Artifacts'
          destination: AzureBlob
          storage: azuresdkartifacts
          containerName: 'azure-sdk-for-net'
          blobPrefix: dependencies